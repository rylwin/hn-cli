load test-helper

@test "__ui_refresh_top_stories" {
  run __ui_refresh_top_stories

  [[ "${lines[0]}" == *"TOP STORIES"* ]]
  [[ "${#lines[@]}" == "21" ]]
  [[ "${lines[11]}" == *"Model-Based Machine Learning Book"* ]]
  [[ "${lines[12]}" == *"score: 128"* ]]
  [[ "${lines[12]}" == *"by: r0f1"* ]]
}
