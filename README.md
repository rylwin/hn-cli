# hn-cli

HackerNews from the commandline, in bash.

Dependencies: `curl`, `jq`

## Usage

`hn`

## Install

System install:

`sudo make install`

Install to any directory:

`PREFIX=/path/to/dir make install`

## Tests

`make test`
