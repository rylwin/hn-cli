util_open_command() {
  if type xdg-open &> /dev/null ; then
    echo "xdg-open"
  else
    echo "open"
  fi
}
