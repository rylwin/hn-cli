__store_cache_dir="$(mktemp --directory /tmp/hn-cli-cache-XXXX)"
declare -ga __story_ids
declare -gA __stories

store_fetch_top_stories() {
  __store_fetch_top_story_ids
  __store_fetch_stories_by_ids "${__story_ids[@]}"
}

store_use_fixtures() {
  log "Using fixtures"
  __base_url="file://${__dir}/../test/fixtures"
}

__store_fetch_stories_by_ids() {
  local ids="$@"
  for id in $ids; do
    __store_get_story $id &
  done
  wait
  for id in $ids; do
    tsv="$(cat "$(__store_story_cache_file $id)" | \
      jq --raw-output '[.title, .by, .score, .time, .url] | @tsv')"
    IFS=$'\t' read -r title by score item_time url <<< "$tsv"
    __stories[$id|title]="$title"
    __stories[$id|by]="$by"
    __stories[$id|score]="$score"
    __stories[$id|time]="$item_time"
    __stories[$id|url]="$url"
  done
}

__store_fetch_top_story_ids() {
  __story_ids=( $(__store_get_top_stories | __store_limit 10 | __store_ids_to_array) )
}

__store_get_story() {
  file="$(__store_story_cache_file $1)"
  curl --silent "${__base_url}/item/${1}.json" > "$file"
}

__store_get_top_stories() {
  echo "$(curl --silent "${__base_url}/topstories.json")"
}

__store_ids_to_array() {
  json_ids=$(cat)
  array=( $( echo "$json_ids" | jq --raw-output '@tsv' ) )
  echo "${array[@]}"
}

__store_limit() {
  read json
  echo "$json" | jq ".[0:$1]"
}

__store_story_cache_file() {
  echo "${__store_cache_dir}/${1}.json"
}
