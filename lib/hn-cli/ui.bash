declare -g __current_selection=0

__tput_underline_on=$(tput smul)
__tput_underline_off=$(tput rmul)
__tput_bold_on="$(tput bold)"
__tput_normal="$(tput sgr0)"

ui_run() {
  __ui_refresh_top_stories
  __ui_ask_about_stories
}

__ui_ask_about_stories() {
  keys="j/k - up/down; enter - read story; r - refresh"
  read -n 1 -p "${__tput_bold_on}What would you like to do?${__tput_normal} (${keys}): " answer
  echo ""
  case "$answer" in
    b|"")
      __ui_browse_current_story
      __ui_ask_about_stories
      ;;
    j)
      __ui_display_story_summaries $(( $__current_selection + 1 ))
      __ui_ask_about_stories
      ;;
    k)
      __ui_display_story_summaries $(( $__current_selection - 1 ))
      __ui_ask_about_stories
      ;;
    r)
      __current_selection=$(( $__current_selection - 1 ))
      __ui_refresh_top_stories
      __ui_ask_about_stories
      ;;
    q)
      echo "${__tput_bold_on}Goodbye!${__tput_normal}"
      ;;
    *)
      echo "You chose $answer. But I can't do that yet."
      sleep 1
      __ui_display_story_summaries $(( $__current_selection - 1 ))
      __ui_ask_about_stories
      ;;
  esac
}

__ui_browse_current_story() {
  id="$(__ui_current_story_id)"
  url="${__stories[$id|url]}"
  echo ""
  echo "Opening '${url}'..."
  eval "$(util_open_command)" "$url"
}

__ui_build_story_summary_by_id() {
  local index="$1" id="$2" buffer=""
  local title="${__stories[$id|title]}"
  local by="${__stories[$id|by]}"
  local score="${__stories[$id|score]}"
  local item_time="${__stories[$id|time]}"

  if [[ "$__current_selection" == "$index" ]]; then
    buffer="${buffer}${__tput_bold_on}"
  fi
  buffer="${buffer}$(printf "%3s: %s\n" "$index" "${__tput_underline_on}$title${__tput_underline_off}")"
  buffer="${buffer}\n$(printf "%3s  score: %s by: %s at: %s\n" "" "$score" "$by" "$item_time")"
  buffer="${buffer}$(printf "\n")"
  if [[ "$__current_selection" == "$index" ]]; then
    buffer="${buffer}${__tput_normal}"
  fi
  echo -e "$buffer"
}

__ui_current_story_id() {
  index=$(( $__current_selection - 1 ))
  echo "${__story_ids[$index]}"
}

__ui_display_story_summaries() {
  __current_selection=$1
  local index=1
  output="$(tput clear)${__tput_bold_on}TOP STORIES${__tput_normal}\n\n"
  for id in ${__story_ids[@]}; do
    output="${output}$(__ui_build_story_summary_by_id "$index" "$id")\n"
    index=$(( index + 1 ))
  done
  echo -e "$output"
}

__ui_refresh_top_stories() {
  echo -n "$(tput clear)"
  echo -n "Fetching latest stories..."
  store_fetch_top_stories
  __ui_display_story_summaries 1
}
