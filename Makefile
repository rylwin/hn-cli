NAME=hn-cli
$(eval VERSION := $(shell cat ./lib/$(NAME)/VERSION))

DIRS=etc lib bin
INSTALL_DIRS=`find $(DIRS) -type d 2>/dev/null`
INSTALL_LIB_DIRS=`find lib/* -type d | sort -r 2>/dev/null`
INSTALL_FILES=`find $(DIRS) -type f 2>/dev/null`

PREFIX?=/usr/local

test:
	bats -r test

tag:
	git tag v$(VERSION)
	echo "\`git push\` to submit new tag v$(VERSION) to origin"

release: tag

install:
	for dir in $(INSTALL_DIRS); do mkdir -p $(PREFIX)/$$dir; done
	for file in $(INSTALL_FILES); do cp $$file $(PREFIX)/$$file; done

uninstall:
	for file in $(INSTALL_FILES); do rm -f $(PREFIX)/$$file; done
	for dir in $(INSTALL_LIB_DIRS); do rmdir $(PREFIX)/$$dir; done

.PHONY: test tag release install uninstall
